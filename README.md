# Vicepresidencia del Estado Plurinacional de Bolivia (2006 - 2019)

## Discursos, ponencias y documentos de Alvaro García Linera

- [Bolivia: 25 años de democracia (2007)](docs/discursos_ponencias_1-2.pdf)

    Entrevista: edición homenaje "25 años de democracia" de Diario La Razón.

- [El hombre de Estado y el hombre común (2008)](docs/discursos_ponencias_2.pdf)

    Inauguración de exposiciones de postulantes a cargos acéfalos del Poder Judicial.

- [Cómo se derrotó al golpismo Cívico-Prefectural (2008)](docs/discursos_ponencias_3-2.pdf)

- [Los tres pilares de la nueva Constitución Política del Estado (2008)](docs/discursos_ponencias_4.pdf)

    Estado Plurinacional, Economía Estatal y Estado Autonómico.

- [Del Estado neoliberal al Estado plurinacional autonómico y productivo (2008)](docs/discursos_ponencias_5.pdf)

- [El papel del Estado en el Modelo Nacional Productivo (2009)](docs/discursos_ponencias_6.pdf)

- [El Estado Plurinacional (2009)](docs/discursos_ponencias_7.pdf)

- [Del Estado aparente al Estado integral (2010)](docs/discursos_ponencias_8.pdf)

    La construcción democrática del socialismo comunitario.

- [¿Qué es la Nación? (2013)](docs/que_es_la_nacion-1.pdf)

- [9 tesis sobre el capitalismo (2013)](docs/9_tesis_sobre_el_capitalismo-2.pdf)

- [Nación y Mestizaje (c.2013)](docs/nacion_y_mestizaje.pdf)

- [El Vicepresidente describe el nuevo campo político en Bolivia (2014)](docs/el_nuevo_campo_politico-2.pdf)

- [Discurso en la Sesión de Honor de la Asamblea Legislativa Plurinacional por el 189 aniversario de la independencia de Bolivia (2014)](docs/discurso-vpr-06082014-2.pdf)

- [¿Fin de ciclo progresista o proceso por oleadas revolucionarias? (2016)](docs/fin_de_ciclo-2.pdf)

## Ediciones Vicepresidencia del Estado Plurinacional 

### Libros y publicaciones del vicepresidente Alvaro García Linera y otros autores.

- [¿Qué es una Revolución?](docs/revolucion-2.pdf)

    De la Revolución Rusa de 1917 a la revolución en nuestros tiempos.
    Álvaro García Linera, 2017.

- [Descolonización en Bolivia. Cuatro ejes para comprender el cambio.](docs/4-ejes.pdf)

    Patricia Chávez y otros, c.2010.

- [Biografía de Alvaro García Linera](docs/biografia_alvaro_garcia_linera-3.pdf)

- [Bolívar Echeverría. Antología.](docs/bolivar_echeverria.pdf)

    Crítica de la modernidad capitalista.
    Bolivar Echeverría.
 
- [Bolivia en movimiento](docs/bolivia_en_movimiento.pdf)

    Movimientos sociales - Subalternidades - Hegemonías.
    Pablo Iglesias e Iñigo Errejón, 2014

- [Memoria. Conferencia sobre el derecho a la consulta de los pueblos indígenas.](docs/consulta-2.pdf)

    Varios Autores, 2012

- [Democracia, estado, nación.](docs/democracia-estado-nacion-web-2.pdf)

    Álvaro García Linera.

- [Despatriarcalizar para descolonizar la gestión pública](docs/depatriarcalizacion_rev.pdf)

    Patricia Chavez y otras.

- [Injerencia de los Estados Unidos en Bolivia](docs/desclasificados.pdf)

    Documentos desclasificados por el Departamento de Estado de los Estados Unidos.

- [El marxismo en América Latina](docs/marxismo-en-latinoamerica-web.pdf)

    Nuevos caminos al comunismo.
    Bruno Bosteels, 2013.

- [Más allá del capital](docs/mas_alla_del_capital.pdf)

    Hacia una teoría de la transición.
    István Mészáros, 2010.
 
- [Socialismo comunitario](docs/socialismo_comunitario-2.pdf)

    Un horizonte de época.
    Alvaro García Linera, 2015.

- [Conocimientos y sujetos sociales](docs/zemelman-2.pdf)

    Contribución al estudio del presente.
    Hugo Zemelman.

- [Las tensiones creativas de la Revolución](docs/tensiones_revolucion.pdf)

    La quinta fase del Proceso de Cambio.
    Alvaro García Linera.

- [¡Bienvenidos a tiempos interesantes!](docs/zizek-1-2.pdf)

    Slavoj Žižek, 2011.

- [Separatismo en Bolivia / Terrorismo separatista en Bolivia](docs/terrorismo_separatista.pdf)

    Informe Conclusivo de la Comisión Especial Multipartidaria de Investigación de los Hechos y Atentados Acaecidos en la Ciudad de Santa Cruz de la Sierra, 2009.

- [Informe conclusivo](docs/terrorismo-web-2.pdf)

    Informe Conclusivo de la Comisión Especial Multipartidaria de Investigación de los hechos y atentados acaecidos en la ciudad de Santa Cruz de la Sierra, 2009

- [Escritos para la transición](docs/samir_amin_final.pdf)

    Samir Amin, 2010.

- [1492: el encubrimiento del otro](docs/dussel_.pdf)

    Hacia el origen del "mito de la modernidad".
    Enrique Dussel. Edición corregida y aumentada, 2008.

- [El "oenegismo", enfermedad infantil del derechismo](docs/el-oenegismo.pdf)

    Alvaro Garcia Linera, 2011.

- [Estado, democracia y socialismo](docs/estado_democracia_y_socialismo-1-2.pdf)

    Alvaro Garcia Linera, 2015.

- [Geopolítica de la Amazonia](docs/geopolitica_de_la_amazonia.pdf)

    Poder hacendal-patrimonial y acumulación capitalista.
    Alvaro García Linera, 2013.

- [Apuntes para un Estado Plurinacional](docs/ximena_final.pdf)

    Ximena Soruco Sologuren, 2008.

- [El sentido de la historia y las medidas geopolíticas de capital](docs/iiicab_sentido.pdf)

    Jorge Veraza Urtuzuástegui, 2013.

- [Las empresas del Estado. Patrimonio colectivo del pueblo boliviano.](docs/las_empresas_del_estado.pdf)

    Alvaro García Linera, 2013.

## Entrevistas a Alvaro García Linera

- [Entrevista en "La Jornada" de México, por Luis Hernández Navarro. 2012.](docs/entrevista_la_jornada_de_mexico.pdf)

    Entrevista a Álvaro García Linera: el pueblo boliviano y la revolución social.

- [Entrevista en "El Desconcierto", por Rodrigo Ruiz. 2015.](entrevista_el_desconcierto-2.pdf)

    Entrevista a Álvaro García Linera: Socialismo, comunidad e integración.

- [Entrevista en "La Tercera" de Chile, por Alejandro Tapia. 2015.](docs/entrevista_la_tercera_ok.pdf)

    Entrevista a Álvaro García Linera: mar para Bolivia y gobierno de Morales.

# Seminarios "Pensando el mundo desde Bolivia"

- [Primer seminario](docs/pensando_elmundo.pdf)

    Toni Negri y otros, 2010.

- [Segundo seminario](docs/tomo_ii_pensado.pdf)

    Slavoj Žižek y otros, 2011.

- [Separata del segundo seminario](docs/separata.pdf)

## Cuadernos de Reflexión (2007-2008)

- [Justicia = ¿Poder Judicial?](docs/cuadernos_de_reflexion_1.pdf)

    Necesidades y alternativas de cambio.
    Alberto Binder y otros, 2007.

- [El Proceso constitucional en Bolivia](docs/cuadernos_de_reflexion_2.pdf)

    Perspectivas desde el nuevo constitucionalismo latinoamericano.
    Rubén Martínz Dalmau y otros, 2008.

## Proceso Constituyente (2006-2009)

Compilación de documentos originales del proceso Constituyente.

- [Preámbulo de la Enciclopedia Histórica Documental del Proceso Constituyente Boliviano](docs/preambulo.pdf)

- Tomo 1. En los umbrales de la Asamblea Constituyente

    - [Volumen 1: Antecedentes e inicio](docs/tomoi_v1.pdf)
    - [Volumen 2: Antecedentes sobre el reglamento](docs/tomoi_v2.pdf)

- Tomo 2: Visión de País: Exposición de las Representaciones Políticas

    - [Volumen 1: Deliberaciones sobre propuestas de Visión de País](docs/tomoii_v1.pdf)
    - [Volumen 2: Deliberaciones sobre propuestas de Visión de País (II)](docs/tomoii_v2.pdf)
    
- Tomo 3: Informes por comisiones

    - [Volumen 1: La construcción del texto constitucional](docs/tomoiii_v1.pdf)
    - [Volumen 2: La construcción del texto constitucional (II)](docs/tomoiii_v2.pdf)

- [Tomo 4: Sesiones Deﬁnitorias Sucre - La Paz - Oruro](docs/tomoiv.pdf)

- [Tomo 5: Congreso Constituyente](docs/tomov.pdf)

    Del texto Constitucional de Oruro al Referéndum del 25 de enero de 2009.

- [Nueva Constitución Política del Estado](docs/ncpe_cepd.pdf)
    
    _Conceptos elementales para su desarrrollo normativo_
    Idón Moisés Chivi Vargas (coordinador), 2010

## Documentos electorales y de gestión

- [Resultados electorales 2005-2009](docs/ciclo_resultados.pdf)

    _Primer ciclo de gobierno indígena en Bolivia._
    
- [Resultados electorales del Referendum revocatorio 2008](docs/referendum_revocatorio.pdf)
- [Resultados electorales del referendum constituyente 2009](docs/referendum_constituyente.pdf)
- [Elecciones departamentales y municipales 2010](docs/elecciones_locales_2010.pdf)
- [Modelo económico plurinacional productivo 2010](docs/presentacion-vpr-19092010.pdf)


## Publicaciones periódicas

### Periódico Plurinacional (2011)

Periódico de circulación nacional, libre y gratuita.

- [Número 1](docs/periodico_no_1.pdf)
- [Número 2](docs/periodico_no_2.pdf)
- [Número 3](docs/pluri_3-2.pdf)
- [Número 4](docs/pluri_4.pdf)
- [Número 5](docs/plurinacional_no_5-2.pdf)
- [Número 6](docs/plurinacional_6.pdf)
- [Número 7](docs/pluri_7.pdf)
- [Número 8](docs/plurinacional_8.pdf)
- [Número 9](docs/plurinacional_9m.pdf)

### Revista La Migraña (2013-2018)

Revista de análisis político.

- [Número 1](docs/migrana-01.pdf)
- [Número 2](docs/migrana-02.pdf)
- [Número 3](docs/migrana-03.pdf)
- [Número 4](docs/migrana-04.pdf)
- [Número 5](docs/migrana-05.pdf)
- [Número 6](docs/migrana-06.pdf)
- [Número 7](docs/migrana-07.pdf)
- [Número 8](docs/migrana-08.pdf)
- [Número 9](docs/migrana-09.pdf)
- [Número 10](docs/migrana-10.pdf)
- [Número 11](docs/migrana-11.pdf)
- [Número 12](docs/migrana-12.pdf)
- [Número 13](docs/migrana-13.pdf)
- [Número 14](docs/migrana-14.pdf)
- [Número 15](docs/migrana-15.pdf)
- [Número 16](docs/migrana-16.pdf)
- [Número 17](docs/migrana-17.pdf)
- [Número 18](docs/migrana-18.pdf)
- [Número 19](docs/migrana-19.pdf)
- [Número 20](docs/migrana-20.pdf)
- [Número 30](docs/migrana-30.pdf)


### Revista de Análisis (2007-2010)

Publicación para el debate e intercambios sobre la coyuntura.

- [Número 1: Reflexiones sobre la coyuntura.](docs/revista_analisis_1.pdf)
- [Número 2: El nuevo modelo económico nacional productivo.](docs/revista_analisis_2.pdf)
- [Número 3: Del Liberalismo al Modelo Nacional Productivo. Los ciclos de la economía boliviana.](docs/revista_analisis_3.pdf)
- [Número 4: El presupuesto general de la Nación 2009.](docs/revista_analisis_4.pdf)
- [Número 5: El socialismo comunitario, un aporte de Bolivia al mundo.](docs/revista_analisis_5.pdf)

### Revista Racismo (2008)

- [Número 1: Racismo y regionalismo en el proceso constituyente.](docs/revista_racismo_1.pdf)
